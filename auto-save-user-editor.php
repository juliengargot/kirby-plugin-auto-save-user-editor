<?php

kirby()->hook('panel.page.update', function($page) {
  $user = site()->user();
  try {
    $page->update(array(
      'editor' => $user,
    ));
  } catch(Exception $e) {
    echo $e->getMessage();
  }
});
